import * as personaggi from "./core/mazzoPersonaggi.js"
import * as distretti from "./core/mazzoDistretti.js"
import * as giocatore from "./core/manoGiocatore.js"


var carteMolo = [
    new distretti.Molo(),
    new distretti.Molo(),
    new distretti.Molo(),
    new distretti.Molo(),
];

var carteVilla = [
    new distretti.VillaPiscina(),
    new distretti.VillaPiscina(),
];

const mazzoDistretti = new distretti.MazzoDistretti(
    ...carteMolo,
    ...carteVilla,
)
mazzoDistretti.shuffle();




const mazzoPersonaggi = new personaggi.MazzoPersonaggi(
    new personaggi.CartaKiller(),
    new personaggi.CartaLadro(),
)
mazzoPersonaggi.shuffle();


var personaggiInMano = new giocatore.CartePersonaggo();
var distrettiInMano = new giocatore.CarteDistretto(mazzoDistretti)

export const manoGiocatore = new giocatore.ManoGiocatore();
manoGiocatore.cartePersonaggo = personaggiInMano
manoGiocatore.carteDistretto = distrettiInMano;

personaggiInMano.push(mazzoPersonaggi[0]);
