
import * as gr from "./lib/gr.js";

export class CartePersonaggo extends gr.EntityList {
    constructor(arg){
        super();
        if(arg){
            this.push(arg);
        }
    }
}

export class CarteDistretto extends gr.EntityList {
    constructor(arg){
        super();
        if(arg instanceof gr.EntityList){
            this.takeFrom(arg, 4);
        }
    }
}

export class ManoGiocatore extends gr.Entity {
    constructor(cartePersonaggo, carteDistretto){
        super();
        if(cartePersonaggo){
            this.cartePersonaggo = cartePersonaggo
        }
        if(carteDistretto){
            this.carteDistretto = carteDistretto
        }
    }

    renderHolder(konvaObj) {
        var layer = new Konva.Layer();
        var imageObj = new Image();
        var promise = new Promise((resolve, reject) => {

            imageObj.onload = () => {
                var cardHolder = new Konva.Image({
                    x: konvaObj.boardWidth/4,
                    y: konvaObj.boardHeight - konvaObj.boardHeight/2,
                    width: konvaObj.boardWidth/2,
                    height: konvaObj.boardHeight/3,
                    image: imageObj,
                    opacity: 0.7,
                });

                layer.add(cardHolder);
                konvaObj.drawLayer(layer);
                resolve()
            }
        })

        imageObj.src = '../assets/img/cardHolder.png';
        return promise;
    }

    renderCard(konvaObj, dove, obj = {}) {
        var layer = new Konva.Layer();
        var imageObj = new Image();
        var promise = new Promise((resolve, reject) => {

            imageObj.onload = () => {
                var card = new Konva.Image(Object.assign({
                    x: konvaObj.boardWidth/3,
                    y: konvaObj.boardHeight - konvaObj.boardHeight/6,
                    width: konvaObj.boardWidth/8,
                    height: konvaObj.boardHeight/5,
                    image: imageObj,
                    opacity: 1,
                }, obj));
                
                layer.add(card);
                konvaObj.drawLayer(layer);
                resolve()
            }
        })

        imageObj.src = '../assets/img/provaMonument.png';
        return promise;
    }

    //moveTo(cosa,dove)
    //
}