import * as gr from "./lib/gr.js";
import Carta from "./carta.js";

export class CartaPersonaggio extends Carta {
    constructor() {
        super (...arguments)
    }
}

export class MazzoPersonaggi extends gr.EntityList {
    constructor(...arg) {
        super(...arg)
    }
}

export class CartaKiller extends CartaPersonaggio {
    constructor() {
        super(1, 'Killer', CartaKiller.prototype.doKiller, 'Uccide un personaggio, che non gioca il turno')
    }

    doKiller() {
        var popup = document.appendChild(document.createElement('div'));
        var pulsanti;

        pulsanti.addEventListener('click', () => {
            var morto = pulsanti.data('morto');
            this.getGameStatus().mainLoop.morto = morto;
            popup.close(); //si deve scrivere
            state.gameLoop.next();
        });
    }
}

export class CartaLadro extends CartaPersonaggio {
    constructor(){
        super(2, 'Ladro', CartaLadro.prototype.doLadro, 'RUba il danaro di un personaggio, appena esso viene svelato')
    }

    doLadro(state) {
        var popup = document.appendChild(document.createElement('div'));
        var pulsanti;

        pulsanti.addEventListener('click', function(){
            var derubato = pulsanti.data('morto');
            var ladro = state.turnoCorrente.currentPlayer

            state.turnoCorrente.morto = morto
            popup.close(); //si deve scrivere
            state.gameLoop.next();
        });
    }
}




