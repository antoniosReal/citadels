import * as gr from "./lib/gr.js";

export default class Carta extends gr.Entity {
    constructor(valore, tipo, azione, descrizione, nome) {
        super()
        Object.assign(this, {
            valore: valore || '',
            tipo: tipo || '',
            azione: azione || azione,
            descrizione: descrizione || '',
            nome: nome || '',
        })
    }
}