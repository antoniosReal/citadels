import * as gameResurces from "../index.js" 

export default class Game {
    constructor() {
        this.boardHeight = 768;
        this.boardWidth = 1024;
        
        this.game = new Konva.Stage({
            container: 'game',
            width: this.boardWidth,
            height: this.boardHeight
        });

        this.createBoard().then(() => {
            gameResurces.manoGiocatore.renderHolder(this)
            this.renderButton(this.boardWidth*0.8, this.boardHeight*0.9,200,50)
        });
    }

    createBoard() {
        this.boardLayer = new Konva.Layer();
        var imageObj = new Image();
        var promise = new Promise((resolve, reject) => {

            imageObj.onload = () => {
                this.board = new Konva.Image({
                    x: 0,
                    y: 0,
                    image: imageObj,
                    width: this.boardWidth,
                    height: this.boardHeight
                });
    
                this.boardLayer.add(this.board);
                this.drawLayer(this.boardLayer);
                resolve()
            }
        })

        imageObj.src = '../assets/img/board.jpg';
        return promise;
    }

    drawLayer(elm) {
        this.game.add(elm);
        elm.draw();
    }

    renderButton(x,y,w,h){
        var btnLayer = new Konva.Layer();
        var imageObj = new Image();
        this.button = new Konva.Text({
            x: x,
            y: y,
            width: w,
            height: h,
            fill: 'blue',
            text: 'Click to card',
            fontSize: 30,
            fontFamily: 'Calibri',
        });

        btnLayer.add(this.button);
        this.drawLayer(btnLayer);

        this.button.on('click touchstart', (e) => {
           gameResurces.manoGiocatore.renderCard(this, "mano")
        });
    }
}

new Game();






// create our shape




