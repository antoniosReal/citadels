import iEvent from "./iEventListener.mjs";

var entities = [];

var gamestatus = {
	entities: entities
}

export function pipe(v){
	return v;
}

export function dummy(v){
	return;
}

export var 	initializeEntities = true;

export function notifyOutput(out = 'log', ...arg){
	console[out](...arg);
}

export var output

export function notify(...arg){
	notifyOutput(output, ...arg);
}

export function croack(...v){
	v = new this.Error(v)
	croack.last = v;
	let out = v.output || output
	if(out){
		notifyOutput(out, ...v);
		return v;
	}
	throw v
}

export class EntityError extends Error{
	constructor(){
		super(...arguments);
		this.arguments = arguments;
	}

}

function getEntityString(){
	return `[Entity ${this.id}]`
}

export function iEntityBase () {
	iEvent.call(this, true)
	var renderEventToken = this.registerEvent('render')
	var id = entities.length
	Object.defineProperties(this, {
		'id': {
			value: id,
			writable: false
		}
	})
	this.toString = getEntityString
	this.croack = croack;
	this.Error = EntityError;
	this.getGameStatus = getGameStatus;
	this.setGameStatus = setGameStatus;
	entities.push(this);
}

function getGameStatus(path){
	path = path.length ? `.${path}` : '';
	notify(`getting status${path} by ${this.id} ${this} ${getEntityString.call(this)}`)
	return new Promise(function(resolve){
		var result = new Function('status', `return status${path}`)(gamestatus)
		resolve(result);
	})
}
function setGameStatus(path, value){
	path = path.length ? `.${path}` : '';
	notify(`setting status${path} by ${id} ${this} ${getEntityString.call(this)}`)
	return new Promise(function(resolve){
		var result = new Function('status', `
			if(typeof status.${path} == "object"){
				return Object.assign(status${path}, ${value})
			}
			status${path} = ${value}
		`)(gamestatus)
		resolve(result);
	})
}




export const Entity = (() => class Entity {
	constructor(){
		iEntityBase.apply(this);
	}
})()

export const EntityList = (() => class EntityList extends Array {
	constructor(){
		super(...arguments);
		if(initializeEntities){
			iEntityBase.call(this);
		}
		this.current = 0;
		this.restart = true;
	}

	pick(v = 1){
		v = this.current + v
		if(this.restart){
			return this[v % this.length];
		}
		switch(true){
			default:
				return this[v];
			case v > this.length:
			case v < 0:
				return this.croack(this[v]);
		}

	}

	next(shift = 1){
		try{
			var current = this.pick(this.current + shift - 1);
			this.current += shift
			return current;
		} catch(value) {
			return;
		}
	}

	back(shift = 1){
		this.next(-shift);
	}

	shuffle(){
		for (let i = 0; i < this.length; i++) {
			let temp = this[i];
			let choosen = Math.trunc(Math.random()*this.length)
			this[i] = this[choosen]
			this[choosen] = temp
		}
		return this;
	}

	takeFrom(list, howMuch = 1, random = true){
		if(random){
			let result = list.get(howMuch);
			this.put(...result);
		} else for (let i = 0; i < howMuch || list.length; i++) {
			let result = list.pop();
			this.push(result);
		}
		return this;
	}

	get(howMuch = 1){
		var result = [];
		for(let i = 0; i < howMuch; i++){
			let choosen = this.splice(Math.random() * this.length, 1)
			result.push(choosen[0]);
		}
		return result.length == 1? result[0]: result;
	}

	put(...v){
		for(let i = 0; i < v.length; i++){
			this.splice(Math.random() * this.length, 0, v[i]);
		}
	}
})();
([
	'splice',
	'filter',
	'map'
]).forEach(function(k){
	EntityList.prototype[k] = function(){
		initializeEntities = false;
		var result = Array.prototype[k].apply(this, arguments);
		initializeEntities = true;
		return Array.from(result);
	}
	EntityList.prototype['_' + k] = function(){
		var result = Array.prototype[k].apply(this, arguments);
		this.length = 0;
		result.filter((v) => this[this.length] = v)
		return this;
	}
})


export const GameLoop = (() => class GameLoop extends EntityList {
	constructor(){
		super(...arguments);
		var status = 'stop'
		Object.defineProperty(this, 'status', {
			get: () => status
		})
	}

	next(v){
		if(status == 'stop'){
			return this.croack('next on stopped loop')
		}
		this.dispatchEvent(new Event('tick'))
		this.dispatchEvent(new Event(`tick ${this.current}`))
		var next = EntityList.prototype.next(v);
		switch(true){
			case next instanceof GameLoop:
				next = next.start();
				break;
			case next instanceof EntityList:
			case next instanceof Entity:
				next = next.action();
				break;
			case next instanceof Function:
				next = next();
				break;
		}
		this.dispatchEvent(new Event('aftertick'));
		return next;
	}

	start(){
		this.dispatchEvent(new Event('started'));
		this.next();
	}

	stop(){
		this.dispatchEvent(new Event('stopped'));
	}
})()

export const GameRuntime = ((gameIdPtr) => class GameRuntime extends Entity {
	constructor(gameName, mainLoop){
		super();
		Object.defineProperty(this, 'name', {
			get: () => gameName
		})
		this[gameIdPtr] = Math.random() * Number.MAX_SAFE_INTEGER;
		gamestatus.mainLoop = mainLoop;

		this.toString = function(){
			return `[GameRuntime ${this[gameIdPtr]}]`
		}
	}

	delete(entity){
		delete entities[entity.id]
	}


})(Symbol())