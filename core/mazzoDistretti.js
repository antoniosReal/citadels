import * as gr from "./lib/gr.js";
import Carta from "./carta.js";


export class CartaDistretto extends Carta {
    constructor(...arg) {
        super(...arg)
    }

}

export class Molo extends CartaDistretto {
    constructor(){
        super(2, 'Shopping', Molo.prototype.doAction, 'molo', 'molo dellaggente', Molo.prototype.render)
    }

    doAction(){

    }

    render(konvaObj) {
        var layer = new Konva.Layer();
        var rect = new Konva.Rect({
            x: 0,
            y: konvaObj.boardHeight / 3 * 2,
            width: konvaObj.boardHeight / 3,
            height: 50,
            fill: 'green',
            stroke: 'black',
            strokeWidth: 4
          });

        layer.add(rect);
        konvaObj.drawLayer(layer);
    }
}

export class VillaPiscina extends CartaDistretto {
    constructor(){
        super(5, 'Lussuoso', VillaPiscina.prototype.doAction, 'villa', 'villa piscina')
    }

    doAction(){

    }
}


export class MazzoDistretti extends gr.EntityList {
    constructor(...arg) {
        super(...arg)
    }
}
